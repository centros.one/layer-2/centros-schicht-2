# Centros Kubernetes Platform

Welcome to the Centros Kubernetes platform. This repository contains a single playbook (`centros.yml`) that deploys all platform-level components necessary to deploy centros' microservice-landscape to a Kubernetes cluster.

For a detailed description of what's in this repository and how to configure deployments, please read [Architecture](#architecture).

To get started, head to the [Quick Start](#quick-start) section.

# Features

- nginx as ingress
- cert-manager for TLS at ingress (LetsEncrypt)
- cert-manager for custom CA issueing PKCS12 certificates for services
- prometheus operator
- kafka and zookeeper
- gitlab
- grafana
- harbor registry
- loki
- integrates grafana with prometheus / loki
- integrates the cluster with GitLab (optional)
- setup multiple cluster-wide pull-secrets for services
- automatically scrape logs and metrics from all services
- scrape Kafka metrics
- deploy to different clusters and namespaces
- extensible (simple Ansible)

# Supported Cloud Providers

- DigitalOcean
- HETZNER
- syseleven
- Azure
- AWS
- IONOS

# Prerequisites

To successfully use this playbook, you need the following tools installed to the control machine:

- Helm
- kubectl
- ansible >= 2.10
- Optional: [Lens Kubernetes IDE](https://k8slens.dev/)

## Configure python

```bash
pip3 install ansible openshift
```

## Configure Lens

Assuming you have a kubeconfig file somewhere on your system, in Lens either:

- add a new cluster pointing to the kubeconfig file
- add a new cluster pasting the contents of the kubeconfig file

### Setup metrics

Assuming `prometheus` will be enabled for the deployment, you can make all metrics gathered by Prometheus available directly within Lens by configuring the monitoring endpoints:

![Lens Metrics](docs/lens-prometheus.png "Lens Metrics")

# Quick Start

> NOTE: You need a Kubernetes cluster (ideally FRESH / EMPTY or PRE-CONFIGURED from this repository) to work through this successfully.

To run this Playbook against a specific Kubernetes cluster, all you have to do is to point your local tooling (helm, kubectl, Ansible) to the respective cluster.

This can be done by setting the `KUBECONFIG` environment variable to the config file for your cluster.

> NOTE: typically on creating a new cluster, you should be given the option to download its kubeconfig-file to your machine. We assume that you have a kubeconfig in ~/Downloads/centros-kubeconfig.yml

```bash
export KUBECONFIG=~/Downloads/centros-kubeconfig.yml
```

Once you're targetting the correct cluster, simply run this:

```bash
ansible-playbook -i "centros," -t "centros" centros.yml
```

> NOTE: This runs the whole deployment playbook with default configuration (see `defaults.yml`)

## Authentication for centros platform repository

The kafka-part of Centros needs authentication to be pulled. Set your GitLab credentials like this:

```bash
export CENTROS_PLATFORM_REPOSITORY_PASSWORDCENTROS_PLATFORM_REPOSITORY_USER=GITLAB_USER
export CENTROS_PLATFORM_REPOSITORY_PASSWORD=GITLAB_TOKEN  
```

# Only deploy specific parts of the platform

Inside the playbook, components are separated by using `tags`. All components (like `cert-manager`) have a tag called `centros` - so calling ansible with that tag will deploy all components.

Every component has a dedicated tag only for its own scope (like `cert-manager`). You can target specific components by running ansible scoped to a specific tag:

```bash
ansible-playbook -i "centros," -t "prometheus" centros.yml
```

This will only run the prometheus-specific parts of the provisioning playbook.

# Configuration

Default configuration options can be found in `defaults.yml`. This file will be loaded by `centros.yml`. Additional configuration options can be set in multiple ways:

- changing the value in `defaults.yml`
- creating an additional file (e.g. `staging.yml`) containing your configuration and feeding it to ansible when executing the playbook: `ansible -i "centros," -e @staging.yml -t "centros" centros.yml`
- setting environment variables (see table below) before running the playbook
- overwriting variables directly in the playbooks or roles
- injecting configuration by using ansible's `--extra-vars` flag: `ansible-playbook -i "centros," -t "centros" -e "gitlab_registry_username=asdasdasd" centros.yml`

> NOTE: sensitive configuration options like secrets and credentials that don't have a default need to be set before executing the playbook - either via EnvVar or as an Ansible extra-var. The playbook fails when these are not set correctly. You need to specifiy these configs **EVERY TIME** you run the playbook - if you change values for a configuration option (like the Username for Cloudflare), Ansible will reflect this by changing the data saved to Kubernetes. Make sure to keep configuration scoped and available each time you run the playbook or you might get unxepected results

## Configuration options

| Configuration Option        | Description | Environment Variable           | Default  |
| ------------- |:-------------:| -----:|-----:|
| `cert_manager_enabled` | Enable/Disable cert-manager |    `CERT_MANAGER_ENABLED` | `True` |
| `cert_manager_version` | Version of cert-manager to install |    `CERT_MANAGER_VERSION` | `v1.1.0` |
| `cert_manager_letsencrypt_staging_issuer_enabled` | Enable/Disable a staging issuer for LetsEncrypt |    `CERT_MANAGER_LETSENCRYPT_STAGING_ISSUER_ENABLED` | `True` |
| `cert_manager_letsencrypt_prod_issuer_enabled` | Enable/Disable a production issuer for LetsEncrypt |    `CERT_MANAGER_LETSENCRYPT_PROD_ISSUER_ENABLED` | `True` |
| `cert_manager_ca_issuer_enabled` | Enable/Disable a CA issuer to power encryption of inter-service communication |    `CERT_MANAGER_CA_ISSUER_ENABLED` | `False` |
| `cert_manager_ca_issuer_certificate_path` | The path to the CA certificate used to power the CA issuer (NOTE: the path is relative to the role_path of the `cert-manager` role; actual files should be put to `roles/cert-manager/files`) |    `CERT_MANAGER_CA_ISSUER_CERTIFICATE_PATH` | `ca.crt` |
| `cert_manager_ca_issuer_key_path` | The path to the CA key used to power the CA issuer (NOTE: the path is relative to the role_path of the `cert-manager` role; actual files should be put to `roles/cert-manager/files`) |    `CERT_MANAGER_CA_ISSUER_KEY_PATH` | `ca.key` |
| `cert_manager_route53_enabled` | Enable/Disable Route53 integration |    `CERT_MANAGER_ROUTE53_ENABLED` | `False` |
| `cert_manager_route53_access_key_id` | Route53 access key id |    `CERT_MANAGER_ROUTE53_ACCESS_KEY_ID` | `-` |
| `cert_manager_route53_secret_access_key` | Route53 integration secret access key |    `CERT_MANAGER_ROUTE53_SECRET_ACCESS_KEY` | `-` |
| `gitlab_enabled` | Enable GitLab      |    `GITLAB_ENABLED` | `False` |
| `gitlab_domain`      | The domain for GitLab | `GITLAB_DOMAIN` | `centros_platform_domain` |
| `gitlab_kubernetes_cluster_integration_enabled` | Enable the integration of the current cluster with a GitLab repository or group      |    `GITLAB_KUBERNETES_CLUSTER_INTEGRATION_ENABLED` | `False` |
| `gitlab_kubernetes_registry_credentials_enabled` | Enable the creation of the registry credentials (DEPRECATED)      |    `GITLAB_KUBERNETES_REGISTRY_CREDENTIALS_ENABLED` | `False` |
| `harbor_enabled` | Enable Harbor      |    `HARBOR_ENABLED` | `False` |
| `harbor_core_domain`      | The domain for Harbor Core | `HARBOR_CORE_DOMAIN` | `core.harbor.${centros_platform_domain}` |
| `harbor_notary_domain`      | The notary for Harbor Core | `HARBOR_NOTARY_DOMAIN` | `notary.harbor.${centros_platform_domain}` |
| `harbor_admin_password` | The Harbor admin password      |    `HARBOR_ADMIN_PASSWORD` | `harbor123456` |
| `harbor_config_overrides` | Config Overrides for the Harbor HELM Chart | `-` | `-` |
| `kafka_enabled` | Enable Kafka      |    `KAFKA_ENABLED` | `True` |
| `kafka_config_overrides`      | Config Overrides for the Kafka HELM Charts | `-` | `-` |
| `loki_enabled` | Enable/Disable Loki      |    `LOKI_ENABLED` | `False` |
| `nginx_ingress_enabled` | Enable/Disable nginx ingress      |    `NGINX_INGRESS_ENABLED` | `True` |
| `postgres_enabled` | Enable/Disable Postgres      |    `POSTGRES_ENABLED` | `False` |
| `postgres_username` | The username for the default postgres user that will be created on deployment |    `POSTGRES_USERNAME` | `postgres` |
| `postgres_password` | The password for the default postgres password that will be created on deployment |    `POSTGRES_PASSWORD` | `postgres` |
| `prometheus_enabled` | Enable/Disable Prometheus & Grafana      |    `PROMETHEUS_ENABLED` | `True` |
| `pull_secrets_enabled` | Enable/disable the creation of pull secrets in the cluster |    `PULL_SECRETS_ENABLED` | `True` |
| `*_registry_username` | Username for a container registry |    `*_REGISTRY_USERNAME` | - |
| `*_registry_password` | Password for a container registry |    `*_REGISTRY_PASSWORD` | - |
| `*_registry_url` | URL for a container registry |    `*_REGISTRY_URL` | `registry.gitlab.com` |
| `centros_namespace`      | The namespace the centros Kubernetes Platform will be deployed to | `CENTROS_NAMESPACE` | `centros` |
| `centros_platform_domain`      | The domain for this deployment | `CENTROS_PLATFORM_DOMAIN` | `127-0-0-1.nip.io` |
| `in_ci`      | Asserts if running in CI environments      |   `CI` | `False` |
| `centros_letsencrypt_mail` | The email address used for LetsEncrypt certificates      |    `CENTROS_LETSENCRYPT_MAIL` | `info@example.com` |
| `centros_chart_repository` | The chart repository      |    `CENTROS_PLATFORM_REPOSITORY` | `https://'+centros_chart_repository_user+':'+centros_chart_repository_password+'@gitlab.com/ddgix/cloudia-services/helm-charts/kafka-infrastructure.git` |
| `centros_chart_repository_branch` | The chart repository branch    |    `CENTROS_PLATFORM_REPOSITORY_BRANCH` | `v6.0.1-1` |
| `centros_chart_repository_user` | The user for authentication against the chart repository      |    `CENTROS_PLATFORM_REPOSITORY_USER` | `gitlab-ci-user` |
| `centros_chart_repository_password` | The password for authentication against the chart repository      |    `CENTROS_PLATFORM_REPOSITORY_PASSWORD` | `${CI_JOB_TOKEN}` |

# Managing multiple Kubernetes clusters

This playbook works with one cluster at a time. Chances are good that over time there will be multiple clusters to work with (development, staging, customer, ...). Since each of these clusters might have a different configuration, the easiest way to configure and deploy the platform to additional clusters is to clone and edit `defaults.yml` and feed the new config to ansible.

```bash
cp defaults.yml staging.yml
[edit stuff & save]
export KUBECONFIG=~/kubeconfig-staging.yml
ansible-playbook -i "centros," -t "centros" -e @staging.yml centros.yml
```

The new config file can either be managed alongside this repository or elsewhere and then referenced by an absolute path.

# Architecture

This playbook contains all platform-level components for the centros AI Platform. Except `nginx-ingress`, all components are agnostic in that their target-system is `an arbitrary Kubernetes cluster`. `nginx-ingress` expects some form of loadbalancer to exist (which is a given with managed Kubernetes services in the cloud) and extra-configuration will be necessary to make `kubernetes-platform` work on Docker for Desktop or other non-cloud environments.

The order of events in this playbook is the following:

- create platform namespace defined in `centros_namespace`
- deploy `cert-manager`
  - create LetsEncrypt staging provisioner
  - create LetsEncrypt prod provisioner
  - create centros CA provisioner
- deploy `nginx-ingress`
- deploy `loki`
  - included Promtail
- deploy `postgres`
- deploy `prometheus`
  - includes Grafana, NodeExporter, etc
- creare `pull-secrets`
- integrate `gitlab`
  - optional: integrate cluster with GitLab repository/group (disabled by default)

All necessary deployment steps have been worked into the ansible playbook `centros.yml`. Components can be enabled/disabled and configured with [configuration options](#configuratio-options).


## cert-manager

`cert-manager` handles all things SSL and certificates. In this context, it has 2 main purposes:

1. issue (and re-issue) valid certificates from LetsEncrypt (staging or production) for services registering endpoints with `nginx-ingress`
2. issue (and re-issue) valid certificates from a custom CA and make these certificates available in PKCS12 format to all centros services as a Kubernetes secret

> NOTE: the default way to configure cert-manager is to run `ansible-playbook -i "centros," -t "cert-manager" centros.yml`

If `cert_manager_route53_enabled` is `True` (which is the default), the playbook creates a secret called `cert-manager-route53-credentials-secret` in `centros_namespace` - this secret contains the AWS credentials (see [configuration](#configuration) on how to specify these credentials) needed for the Route53 integration.

`cert-manager` is configured to use the route53 solver for the `centros_platform_domain` (defaults to `127-0-0-1.nip.io`).

### Manual steps

```bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install \
  cert-manager jetstack/cert-manager \
  --namespace centros \
  --version v1.1.0 \
  --set installCRDs=true
```

## loki

> NOTE: the default way to configure loki is to run `ansible-playbook -i "centros," -t "loki" centros.yml`

Grafana's loki is being used for centralized logging. This component will install loki alongside promtail to capture cluster-wide logs.

Grafana (as part of [prometheus](#prometheus)) is pre-configured with `loki` as a datasource so logs can easily be explored or worked into diagrams.

## nginx-ingress

> NOTE: the default way to configure nginx-ingress is to run `ansible-playbook -i "centros," -t "nginx-ingress" centros.yml`

In Kubernetes, the ingress controls incoming traffic and allows you to manipulate routing (and more). We're using nginx as the ingress for 2 reasons:

- it's the most used and most stable while still simplest implementation of an ingress and widely adapted (integration work is low)
- it comes with no special configuration / CRD requirements and integrates easily with cert-manager

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install --namespace centros nginx-ingress ingress-nginx/ingress-nginx
```

Check nginx is ready:

```bash
kubectl --namespace centros get services nginx-ingress-ingress-nginx-controller 
```

## centros-platform

This installs Kafka. Our default parameters can be set in `roles/centros-platform/defaults/main.yml` for now.

## postgres

> NOTE: the default way to configure postgres is to run `ansible-playbook -i "centros," -t "postgres" centros.yml`

Postgres serves as a central database instance for all services. Default username and password can be configured using `postgres_username` and `postgres_password`.

Using `postgres_additional_objects` you can provide a list of databases and user-credentials that should be created on top of the defaults.

PostgreSQL can be accessed via port 5432 on the following DNS name from within your cluster:

`postgres-postgresql.centros.svc.cluster.local`

To get the password for "postgres" run:

```bash
export POSTGRES_PASSWORD=$(kubectl get secret --namespace centros postgres-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
```

To connect to your database run the following command:

```bash
kubectl run postgres-postgresql-client --rm --tty -i --restart='Never' --namespace centros --image docker.io/bitnami/postgresql:11.10.0-debian-10-r60 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host postgres-postgresql -U postgres -d postgres -p 5432
```

To connect to your database from outside the cluster execute the following commands:

kubectl port-forward --namespace centros svc/postgres-postgresql 5432:5432 &
PGPASSWORD="$POSTGRES_PASSWORD" psql --host 127.0.0.1 -U postgres -d postgres -p 5432

## prometheus

> NOTE: the default way to configure prometheus is to run `ansible-playbook -i "centros," -t "prometheus" centros.yml`

Prometheus collects a vast number of metrics about the cluster and all running containers out of the box. We're using Prometheus Operator here - bundled with Grafana and all necessary collectors in `kube-prometheus-stack` - instead of the legacy Prometheus stack. That means our deployments will not be tracked/discovered via annotations but instead through so called `ServiceMonitors` which will be created automatically by the HELM chart for centros's services.

Prometheus can be utilized by Lens and other components of the cluster as well.

- https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack#configuration

```bash
kubectl create namespace monitoring
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
helm repo update
helm upgrade -f platform/helm-prometheus-values.yaml --install --namespace monitoring prometheus prometheus-community/kube-prometheus-stack
```

Show config values:

```bash
helm show values prometheus-community/kube-prometheus-stack
```

### TBD: Adding Dashboards to Grafana

- https://grafana.com/grafana/dashboards/10427
- https://grafana.com/grafana/dashboards/13115

## pull-secrets

> NOTE: this is part of the ansible workflow

Pull secrets can be created as part of the playbook. `pull_secrets` (see [configuration](#configuration) or `defaults.yml`) is a list of pull-secrets. A pull-secret needs a registry-url, a username, a password and a name so it can be identified by deployments.

Pull-secrets defined in the configuration will be created in `centros_namespace` (see [configuration](#configuration)) and named according to this pattern: `pull-secret-{pull_secret.name}` (e.g. `pull-secret-centros-demo`).

To use an existing pull-secret for a service, simply specify it in its HELM values:

```bash
imagePullSecrets: 
  - name: pull-secret-centros-demo
```

To configure the credentials for each pull-secret, either put them to a configuration file that you feed to Ansible via `--extra-vars` or set the following environment variables and then run the deployment script: `ansible-playbook -i "centros," -t "pull-secrets" centros.yml`

```bash
export CENTROS_DEMO_REGISTRY_USERNAME=gitlab+deploy-token-12345
export CENTROS_DEMO_REGISTRY_PASSWORD=supersecretpassword1337!
```

> NOTE: `CENTROS_DEMO` is the name of the pull-secret in this example; the name of the variable is arbitrary, just make sure to specify the correct environment variable in the config file (see `defaults.yml`, section `pull_secrets` for examples)

For manual invokation, please refer to [this article](https://chris-vermeulen.com/using-gitlab-registry-with-kubernetes/) to learn what the secret needs to contain to be viable for image pulling.

## gitlab

> NOTE: the default way to configure gitlab is to run `ansible-playbook -i "centros," -t "gitlab" centros.yml`

The Kubernetes cluster can be integrated with GitLab to allow easy deloyments into GitLab-controlled **environments**.

The playbook will guide you (mostly) through the process of connecting your current cluster to GitLab.

> NOTE: this isn't necessary (or useful) with your Docker Desktop Kubernetes cluster

In case you want to do the process manually:

> NOTE: https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster

On the machine you're controlling the cluster from perform the following steps:

- Get the Kubernetes API URL: `kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'`
- Add the API URL from the output to the cluster you're configuring in GitLab
- List secrets: `kubectl get secrets`
- Look for a secret named `default-token-xxxx`
- Run `kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode` where <secret name> should be substituted with your default-token, e.b. `default-token-6x2kk`
- Add the certificate from the output to the cluster you're configuring in GitLab


# Troubleshooting

- `cert-manager` sometimes fails to create Certificate Issuers, outputting something like `Post "https://cert-manager-webhook.centros.svc:443/mutate?timeout=10s": x509: certificate signed by unknown authority\"}]},\"code\":500}\\n'", "reason": "Internal Server Error", "status": 500}` - this can be mitigated by simply running the command again
- missing configuration (typically when dealing with secrets): `"CERT_MANAGER_ROUTE53_ACCESS_KEY_ID and CERT_MANAGER_ROUTE53_SECRET_ACCESS_KEY must be set"` - this means that a necessary configuration option hasn't been set. The errors indicate the Environment Variables to be set before running the playbook but configuration can also be added in a file. `export` the env vars/add config to a vars-file and re-run the command

# References

- https://chris-vermeulen.com/using-gitlab-registry-with-kubernetes/