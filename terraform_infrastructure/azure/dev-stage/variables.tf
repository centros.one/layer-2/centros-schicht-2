# Author: Antony Goetzschel <ago@ccsolutions.io>
# Date: 10.11.20
# Terraform Version: 0.13.2

variable "name" {
  description = "Name of the stage"
  type    = string
  default = "PLACEHOLDERFORNAME"
}

variable "environment" {
  description = "Environment"
  type    = string
  default = "PLACEHOLDERFORENVIRONMENT"
}

variable "tenant_id" {
  description = "Tenant ID"
  type    = string
  default = "PLACEHOLDERTENANTID"
}

variable "subscription_id" {
  description = "Subscription ID"
  type    = string
  default = "PLACEHOLDERSUBSCRIPTIONID"
}

variable "resource_group" {
  description = "Name of the resource group"
  type    = string
  default = "PLACEHOLDERFORRESOURCEGROUP"
}

variable "location" {
  description = "Location of the resources"
  type    = string
  default = "PLACEHOLDERFORLOCATION"
}

variable "network_cidr" {
  description = "IP Address range as CIDR notation"
  type    = string
  default = "192.168.178.0/24"
}

variable "aks_node_count" {
  description = "Number of Nodes for the AKS CLuster"
  type    = string
  default = "PLACEHOLDERCOUNTER"
}

variable "aks_vm_size" {
  description = "AKS Node Flavour"
  type    = string
  default = "PLACEHOLDERVMSIZE"
}

variable "os_disk_size_gb" {
  description = "HDD Size of the AKS Node"
  type    = string
  default = "PLACEHOLDERGB"
}

locals {
  name = var.name
  environment = var.environment
  tenant_id = var.tenant_id
  subscription_id = var.subscription_id
  resource_group = var.resource_group
  location = var.location
  network_cidr = var.network_cidr
  aks_node_count = var.aks_node_count
  aks_vm_size = var.aks_vm_size
  os_disk_size_gb = var.os_disk_size_gb
}