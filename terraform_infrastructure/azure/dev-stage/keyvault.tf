resource "azurerm_key_vault" "keyvault" {
  name = "kv${local.name}apps"
  location = var.location
  resource_group_name = local.resource_group
  tenant_id = local.tenant_id
  soft_delete_enabled = true
  purge_protection_enabled = false
  enable_rbac_authorization = true

  sku_name = "standard"

  tags = {
    environment = local.environment
  }
}