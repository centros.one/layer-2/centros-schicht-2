# Author: Antony Goetzschel <ago@ccsolutions.io>
# Date: 10.11.20
# Terraform Version: 0.13.2

terraform {
  backend "azurerm" {
    resource_group_name   = "Hornbach_KI"
    storage_account_name  = "stgterraformstates"
    container_name        = "stgterraformstates01"
    key                   = "terraform.tfstate"
  }
}
provider "azurerm" {
  version = "=2.35.0"
  features {}
  subscription_id = "38346067-832c-4732-b232-a169ea63bfd4"
  tenant_id = "539c9603-6380-423c-a70c-01acd69c91d5"
}

module "dev_stage" {
  source = "./dev-stage"
  name = "dev-stage"
  environment = "dev"
  tenant_id = var.tenant_id
  subscription_id = var.subscription_id
  resource_group = var.resource_group
  location = var.location

  network_cidr = "10.254.0.0/16"

  aks_node_count = "3"
  aks_vm_size = "Standard_D2_v2"
  os_disk_size_gb = "100"
}