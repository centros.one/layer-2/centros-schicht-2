- name: adding repository
  community.kubernetes.helm_repository:
    name: jetstack
    repo_url: https://charts.jetstack.io
  when: cert_manager_chart is undefined

- name: preparing config
  set_fact: helm_config="{{ helm_values|combine(cert_manager_values, recursive=true) }}"

- name: config
  debug:
    var: helm_config

- name: deploying
  community.kubernetes.helm:
    release_name: "cert-manager"
    chart_ref: "{{ cert_manager_chart | default('jetstack/cert-manager') }}"
    release_namespace: "{{ centros_namespace }}"
    release_values: "{{ helm_config }}"
    update_repo_cache: "{{ cert_manager_chart is undefined }}"
    wait: yes
    chart_version: "{{ cert_manager_version }}"
    wait_timeout: 10m0s

- name: adding route53 credentials secret
  block:
    - name: check conditions
      assert:
        that:
          - cert_manager_route53_access_key_id != ""
          - cert_manager_route53_secret_access_key != ""
        msg: "CERT_MANAGER_ROUTE53_ACCESS_KEY_ID and CERT_MANAGER_ROUTE53_SECRET_ACCESS_KEY must be set"

    - name: adding route53 credentials secret
      community.kubernetes.k8s:
        state: present
        definition: 
          apiVersion: v1
          kind: Secret
          type: Opaque             
          metadata:
            name: "cert-manager-route53-credentials-secret"
            namespace: "{{ centros_namespace }}"     
          stringData:
            access-key-id: "{{ cert_manager_route53_access_key_id }}" 
            secret-access-key: "{{ cert_manager_route53_secret_access_key }}" 
  when: cert_manager_route53_enabled|bool

- name: adding letsencrypt staging issuer
  community.kubernetes.k8s:
    state: present
    definition: 
      apiVersion: cert-manager.io/v1
      kind: ClusterIssuer           
      metadata:
        name: letsencrypt-staging
        namespace: "{{ centros_namespace }}"     
      spec:
        acme:
          server: https://acme-staging-v02.api.letsencrypt.org/directory
          email: "{{ centros_letsencrypt_mail }}"
          privateKeySecretRef:
            name: letsencrypt-staging
          solvers:
            - http01:
                ingress:
                  class: nginx
  when: cert_manager_letsencrypt_staging_issuer_enabled

- name: adding letsencrypt production issuer
  community.kubernetes.k8s:
    state: present
    definition: 
      apiVersion: cert-manager.io/v1
      kind: ClusterIssuer           
      metadata:
        name: letsencrypt-prod
        namespace: "{{ centros_namespace }}"     
      spec:
        acme:
          server: https://acme-v02.api.letsencrypt.org/directory
          email: "{{ centros_letsencrypt_mail }}"
          privateKeySecretRef:
            name: letsencrypt-prod
          solvers:
            # example: cross-account zone management for example.com
            # this solver uses ambient credentials (i.e. inferred from the environment or EC2 Metadata Service)
            # to assume a role in a different account
            # - selector:
            #     dnsZones:
            #       - "{{ centros_platform_domain }}"
            #   dns01:
            #     route53:
            #       region: eu-central-1
            #       # "webhook.cert-manager.io" denied the request: spec.acme.solvers[0].dns01.route53.region: Required value
            #       accessKeyID: "{{ cert_manager_route53_access_key_id }}"
            #       secretAccessKeySecretRef:
            #         name: cert-manager-route53-credentials-secret
            #         key: secret-access-key
            - http01:
                ingress:
                  class: nginx
  when: cert_manager_letsencrypt_prod_issuer_enabled|bool

- name: adding CA for inter-service communication
  block:
    - name: adding CA certificate
      community.kubernetes.k8s:
        state: present
        definition: 
          apiVersion: v1
          kind: Secret
          type: tls             
          metadata:
            name: "centros-ca-key-pair"
            namespace: "{{ centros_namespace }}"     
          data:
            tls.crt: "{{ lookup('file', cert_manager_ca_issuer_certificate_path) | string | b64encode }}" 
            tls.key: "{{ lookup('file', cert_manager_ca_issuer_key_path) | string | b64encode }}" 

    - name: adding CA issuer
      community.kubernetes.k8s:
        state: present
        definition: 
          apiVersion: cert-manager.io/v1
          kind: ClusterIssuer           
          metadata:
            name: centros-ca-issuer
            namespace: "{{ centros_namespace }}"     
          spec:
            ca:
              secretName: centros-ca-key-pair
  when: cert_manager_ca_issuer_enabled|bool
